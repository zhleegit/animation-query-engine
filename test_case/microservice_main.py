from flask import Flask, render_template, jsonify, request, redirect
import json, os, shutil, numpy as np
from os.path import join as pj
import includes.LabelRecommender as LR
import includes.Logger as Logger
import sys
from includes import global_init
app = Flask(__name__)
app, PORT_MAIN, PORT_QE, PORT_SH, PORT_LR = global_init.init(sys,app)


# config constant
DEBUG = True
TOP_K = 3
QE_INIT_DATASOURCE = False


@app.route("/upload_anim", methods=["GET", "POST"])
def api_upload_anims():
    if request.method == "POST":
        file = request.files["file"]
        if file:
            filename = file.filename
            file_save_path = pj(app.config["DIR_UPLOAD_ANIM"], filename)
            file.save(file_save_path)
    return jsonify({"status": "upload success", "path": file_save_path})

@app.route("/upload_slide", methods=["GET", "POST"])
def api_upload_slide():
    if request.method == "POST":
        file = request.files["file"]
        if file:
            filename = file.filename
            file_save_path = pj(app.config["DIR_UPLOAD_SLIDE"],
                            filename)
            file.save(file_save_path)
    return jsonify({"status": "upload success",
                    "path":file_save_path})


@app.route("/list_slides")
def api_list_slides():
    slide_names = [fn for fn in os.listdir(app.config["DIR_UPLOAD_SLIDE"])]
    gen_urls = [f"http://127.0.0.1:{PORT_SH}/gen_v1/{fn}" for fn in slide_names]

    data = [
        {"slide_name": slide_names[i], "gen_url": gen_urls[i]}
        for i in range(len(slide_names))
    ]
    return render_template(
        "list_slides.html",
        data=data,
    )

@app.route('/list_anims')
def api_list_anims():
    anim_names = [fn for fn in os.listdir(app.config["DIR_RUNTIME_ANIM"])]
    gen_urls = [f"http://127.0.0.1:{PORT_LR}/lr/pred_tag/{fn}" for fn in anim_names]

    data = [
        {"anim_name": anim_names[i], "pred_url": gen_urls[i]}
        for i in range(len(anim_names))
    ]
    return render_template(
        "list_anims.html",
        data=data,
    )


@app.route("/")
def index():
    status = {"status": ""}
    return render_template("index.html", data=status)


if __name__ == "__main__":
    app.run(debug=DEBUG, host="127.0.0.1", port=PORT_MAIN)
