import numpy as np
from transformers import pipeline, AutoModel , AutoTokenizer
model = AutoModel.from_pretrained('distilbert-base-uncased')
tokenizer = AutoTokenizer.from_pretrained('distilbert-base-uncased')
model = pipeline('feature-extraction', model=model , tokenizer=tokenizer)


def get_feature(text, array=True):
    feat = model(text)[0][-1]
    if array:
        return np.array(feat)
    return feat



def sim(vec1,vec2):
    return np.dot(vec1,vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2))


