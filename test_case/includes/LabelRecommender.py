## Usage ################################
# 
#  prepare files:
#   - saved/tag_recommend.model
#   - tokenizers.pickle
# 
#  run:
#   - runtime_load_model()
#   - runtime_pre_tag_from_src(gif path)
#   - runtime_finetune()
# 
#  output:
#   - after `runtime_pred_tag_from_src(src)`,
#     return a list of tags
# 
#########################################


import tensorflow as tf

from tensorflow.keras import datasets, layers, models
import os, json, imageio, pickle
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
from os.path import join as pj

from tensorflow.python.ops.gen_math_ops import Log
from includes import Logger

datetime_str = datetime.now().strftime("%Y%m%d-%H%S")


RUNTIME_DATA_DIR = pj("datasource", "runtime", "anim_repo")
RUNTIME_SRC_TAGS = pj("datasource", "runtime", "src_tags.json")
RUNTIME_INCLUDES = 'includes'

# if exceed (N_LABEL-1) labels, belong to 0
N_LABEL = 128
N_RECOMMEND_LABEL = 10
resize_shape = (50, 50, 3)

# model and tokenizers positions
model_base = "saved_model/tag_recommend.model"
model_tuned = f"saved_model/tag_recommend_{datetime_str}_.model"
pickle_base = "tokenizers.pickle"
pickle_tuned = f"tokenizers_{datetime_str}_.pickle"

# runtime parameters
model_base = pj(RUNTIME_INCLUDES , model_base)
model_tuned = pj(RUNTIME_INCLUDES , model_tuned)
pickle_base = pj(RUNTIME_INCLUDES , pickle_base)
pickle_tuned = pj(RUNTIME_INCLUDES , pickle_tuned)
TAG_FILTER = ['NULL' , 'male', 'female']



### prepare training data
def get_train_pairs(src_tags):
    tk_tag_to_idx, tk_idx_to_tag = get_tks(src_tags)
    x, y = [], []
    for src, tags in src_tags.items():
        img = imageio.imread(src)
        img_resized = np.resize(img, resize_shape)
        for tag in tags:

            if tag not in tk_tag_to_idx:
                idx = 0
            else:
                idx = tk_tag_to_idx[tag]

            x.append(img_resized)
            y.append(idx)

    x, y = np.array(x), np.array(y)
    x = x / 255.0
    y = y.reshape((-1, 1))
    return x, y, tk_tag_to_idx, tk_idx_to_tag


def get_tks(src_tags):
    global try_tag_count
    """
    limit tag to N_LABEL-1
    tk_tag_to_idx, tk_idx_to_tag
    """
    all_token = []
    tk_tag_to_idx = {"NULL": 0}
    tk_idx_to_tag = {0: "NULL"}
    for src, tags in src_tags.items():
        for tag in tags:
            all_token.append(tag)

    tag_set_descending = sorted(
        [[tag, count] for tag, count in Counter(all_token).items()], key=lambda x: -x[1]
    )
    if len(tag_set_descending) > N_LABEL - 1:
        # remain the (N_LABEL-1) most frequent tag
        # e.g. (n=128, len(tag_set)=128)
        #  current len(tag_set)=127
        #  `0` is used to store OTHER TAGs
        tag_set_descending = tag_set_descending[: N_LABEL - 1]

    # save plot, with and without gender
    save_tag_count_diagram(tag_set_descending, True)
    save_tag_count_diagram(tag_set_descending, False)

    # initialize tokenizers (bidirections)
    for i in range(len(tag_set_descending)):
        tag, freq = tag_set_descending[i]
        tk_tag_to_idx[tag] = i + 1
        tk_idx_to_tag[i + 1] = tag

    return tk_tag_to_idx, tk_idx_to_tag


def save_tag_count_diagram(tag_count, include_gender=True, top_n=30):
    fn = "tag_count_plot" + ("" if include_gender else "_no_gender_") + ".svg"

    if not include_gender:
        tag_count = [
            t for t in tag_count if "male" != t[0].lower() and "female" != t[0].lower()
        ]
    top_n = top_n if len(tag_count) >= top_n else len(tag_count)

    tags = [t[0] for t in tag_count[:top_n]]
    tags_idx = range(len(tags))
    counts = [t[1] for t in tag_count[:top_n]]
    plt.clf()
    plt.bar(x=tags_idx, height=counts)
    plt.xticks(tags_idx, tags, rotation=90)
    plt.savefig(fn, pad_inches=0.8, bbox_inches="tight")
    print(f"save {fn}")


def train(x, y):
    #    model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=resize_shape))
    model = models.Sequential()
    model.add(layers.Conv2D(32, (3, 3), activation="relu", input_shape=resize_shape))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation="relu"))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation="relu"))
    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation="relu"))
    model.add(layers.Dense(N_LABEL))
    model.summary()
    model.compile(
        optimizer="adam",
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        metrics=["accuracy"],
    )

    history = model.fit(x, y, epochs=200, validation_split=0.1, verbose=2)
    return history, model


def save_model(
    model_fn=None, model=None, pickle_fn=None, tk_tag_to_idx=None, tk_idx_to_tag=None
):
    model.save(model_fn)
    with open(pickle_fn, "wb") as f:
        pickle.dump([tk_tag_to_idx, tk_idx_to_tag], f)


def load_model(model_dir=model_base, pickle_fn=pickle_base):
    model = tf.keras.models.load_model(model_dir)
    with open(pickle_fn, "rb") as f:
        [tk_tag_to_idx, tk_idx_to_tag] = pickle.load(f)
    return model, tk_tag_to_idx, tk_idx_to_tag


def pred_tags(model, tk_idx_to_tag, src):
    img = imageio.imread(src)
    x = np.resize(img, resize_shape)
    x = np.array([x]) / 255.0
    recommend_tags_logit = model.predict(x)
    recommend_tags_sorted = sorted(
        list(zip(range(N_LABEL), recommend_tags_logit[0].tolist())), key=lambda x: -x[1]
    )
    recommend_tags_idx = [t[0] for t in recommend_tags_sorted[:N_RECOMMEND_LABEL]]
    recommend_tags = [tk_idx_to_tag[t] for t in recommend_tags_idx]
    return recommend_tags


## Test case
def run_test():
    with open(RUNTIME_SRC_TAGS, "r", encoding="utf-8") as f:
        src_tags = json.load(f)

    x, y, tk_tag_to_idx, tk_idx_to_tag = get_train_pairs(src_tags)
    history, model = train(x, y)
    save_model(
        model_fn=model_base,
        model=model,
        pickle_fn=pickle_base,
        tk_tag_to_idx=tk_tag_to_idx,
        tk_idx_to_tag=tk_idx_to_tag,
    )

    model_ld, tk_tti, tk_itt = load_model()

    for i, src in enumerate(src_tags.keys()):
        if i > 100:
            break
        if not (
            set(pred_tags(model, tk_idx_to_tag, src))
            == set(pred_tags(model_ld, tk_itt, src))
        ):
            print(i)
            break
    print("test ok")


## Runtime
model, tk_tag_to_idx, tk_idx_to_tag = None, None, None


def runtime_load_model():
    global model, tk_tag_to_idx, tk_idx_to_tag
    Logger.printLog("start loading Labeling model")
    model, tk_tag_to_idx, tk_idx_to_tag = load_model()
    Logger.printLog("Labeling model loaded")


def runtime_pred_tag_from_src(src_img, tag_filter = True):

    tags = pred_tags(model, tk_idx_to_tag, src_img)

    if tag_filter:
        tags_out = [t for t in tags if t not in TAG_FILTER]
        return tags_out
    

    return tags


def runtime_finetune():
    global model, tk_tag_to_idx, tk_idx_to_tag, model_base, model_tuned

    with open(RUNTIME_SRC_TAGS, "r", encoding="utf-8") as f:
        src_tags = json.load(f)

    Logger.printLog(f"get train data from {src_tags}")
    x, y, tk_tag_to_idx, tk_idx_to_tag = get_train_pairs(src_tags)
    Logger.printLog("start training")
    history, model = train(x, y)
    Logger.printLog("Training done, start saving model")
    save_model(
        model_fn=model_tuned,
        model=model,
        pickle_fn=pickle_tuned,
        tk_tag_to_idx=tk_tag_to_idx,
        tk_idx_to_tag=tk_idx_to_tag,
    )
    Logger.printLog(
        f"new model saved as saved_model/tag_recommend_{datetime_str}_.model"
    )
    model_base = model_tuned
    pickle_base = pickle_tuned
    Logger.printLog("use new model source as base")
    load_model()
