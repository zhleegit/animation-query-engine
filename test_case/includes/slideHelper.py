import os
import glob

from includes import Logger
import numpy as np
import pandas as pd
import shutil

from PIL import Image
from time import sleep
from pptx import Presentation
from pptx.enum.shapes import MSO_SHAPE_TYPE


ppt_fn = 'sample_input.pptx'
sample_img1 = 'sample1.png'
sample_img2 = 'sample2.png'
sample_img2 = "init\\init_dataset\\human component 30.gif"

prs_width, prs_height = None, None
dup_texts = {}

show_log = False

TITLE_LAYOUT = 0
BULLET_LAYOUT = 1
TITLE_ONLY_LAYOUT = 5
BLANK_LAYOUT = 6

# tags for injecting processors
TAG_TEXT_TO_SRC = 'texts_to_srcs'



"""
Presentation
-- slides: collection object, accessed by idx

    -- shapes : collection object, accessed by idx, insert object with 
        add_picture, add_shape, add_table etc. 
        see doc: https://python-pptx.readthedocs.io/en/latest/api/shapes.html#pptx.shapes.shapetree.SlideShapes
        
        -- shape:
            https://python-pptx.readthedocs.io/en/latest/api/shapes.html#pptx.shapes.autoshape.Shape

    -- placeholders : collection object, iterable, but has no method to add object
        see doc: https://python-pptx.readthedocs.io/en/latest/api/slides.html#pptx.shapes.shapetree.SlidePlaceholders
"""


def iter_textframed_shapes(shapes):
    """Generate shape objects in *shapes* that can contain text.

    Shape objects are generated in document order (z-order), bottom to top.
    """
    for shape in shapes:
        # recurse on group shapes---
        if shape.shape_type == MSO_SHAPE_TYPE.GROUP:
            group_shape = shape
            for shape in list(group_shape.shapes):
                yield shape
            continue

        # otherwise, treat shape as a "leaf" shape---
        if shape.has_text_frame:
            yield shape


def get_slide_text(sid, slide):

    texts = []
    flat_shapes = list(iter_textframed_shapes(slide.shapes))
    for i, shape in enumerate(flat_shapes):
        # for i,shape in enumerate(slide.shapes):
        # if not shape.has_text_frame:
        #     continue
        if not hasattr(shape, 'text'):
            continue
        if len(shape.text) <= 0:
            continue
        if 'Ⅲ' in shape.text:
            continue
        if shape.text not in texts:
            texts.append(shape.text.replace('\n', ' '))

    # print(f'sID {sid} ',texts)
    return texts


def get_imgs_by_texts(texts):
    """
    Test algorithm

    must summerize all sentences, 
    then insert images related to these texts"""
    return [sample_img1, sample_img2]  # debug: sample images


def insert_imgs_to_slide(slide, fn_imgs):
    for i, fn_img in enumerate(fn_imgs):
        # debug: show input animiation path
        if show_log:
            Logger.printLog(f' : {fn_img}')

        new_w, new_h = get_same_ratio_resize(fn_img)
        slide.shapes.add_picture(fn_img,
                                 left=0,
                                 top=500,
                                 width=new_w,
                                 height=new_h)


def get_same_ratio_resize(fn_img):
    """
    aligning policy
        h>w
            new_h <- prs_h/2

        h<w
            new_w <- prs_w/3
    """
    im = Image.open(fn_img)
    w, h = im.size
    wh_rate = w/h

    prs_h2, prs_w3 = prs_height/2, prs_width/3

    if h > w:
        new_h = prs_h2
        new_w = wh_rate*new_h
    else:
        new_w = prs_w3
        new_h = new_w * (1/wh_rate)

    return int(new_w), int(new_h)


def generate_ppt_from_2D_list(fn_in='sample.pptx',
                              fn_out='sample_exp_out.pptx',
                              text_processors={
                                  TAG_TEXT_TO_SRC:None
                                  },
                              save_img=False):
    """
    query_algorithm is encapsulated outside, and will be injected by App.py
    """
    global prs_width, prs_height
    prs = Presentation(fn_in)
    prs_width, prs_height = prs.slide_width, prs.slide_height

    for i, slide in enumerate(prs.slides):
        texts = get_slide_text(i, slide)

        if len(texts) <= 0:
            continue
        

        fn_imgs = text_processors[TAG_TEXT_TO_SRC](texts)

        if save_img:
            save_img_per_slide(s_fn=fn_in, sid=i+1, fn_imgs=fn_imgs)
        else:
            insert_imgs_to_slide(slide, fn_imgs)

    if save_img:
        return None
    prs.save(fn_out)
    Logger.printLog(f"output file \"{fn_out}\" has been generated! ")
    # os.startfile(fn_out)


def save_img_per_slide(s_fn, sid, fn_imgs):
    
    same_name_dir = s_fn.replace('.pptx', '')
    for fn in fn_imgs:
        sys_out_dir = os.path.join(same_name_dir, f'sys_out_{sid}')
        if not os.path.exists(sys_out_dir):
            os.mkdir(sys_out_dir)

        shutil.copy2(fn, sys_out_dir)


