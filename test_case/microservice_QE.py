from logging import DEBUG
from flask import Flask, render_template, jsonify, request, redirect
import json, os, shutil, numpy as np
from os.path import join as pj
import includes.QueryEngine as QE
from includes import global_init

import includes.Logger as Logger
import sys

QE_initialized = False

app = Flask(__name__)
app, PORT_MAIN, PORT_QE, PORT_SH, PORT_LR = global_init.init(sys,app)


# config constant
TOP_K = 3
QE_INIT_DATASOURCE = False
MAIN_SLIDE_NAME = None
DEBUG = True


def init_server():
    global QE_initialized

    if QE_INIT_DATASOURCE:
        QE.runtime_init_datasource()
    elif not QE_initialized:
        QE.runtime_init()
    QE_initialized = True


@app.route('/texts_to_srcs', methods = ['POST'])
def api_texts_to_srcs():

    json_data = request.json
    texts = json_data["texts"]
    if DEBUG:
        Logger.printLog('QE: receive')
        print(texts)


    topk_srcs = QE.runtime_get_topk_pairs_from_multi_sentences(texts,
                                                k=TOP_K)
    if DEBUG:
        Logger.printLog('QE: return')
        print(topk_srcs)
    
    content = {'srcs': topk_srcs}
    return jsonify(content)





if __name__ == "__main__":
    init_server()
    QE.show_log = DEBUG
    app.run(debug=DEBUG, host="127.0.0.1", port=PORT_QE)
