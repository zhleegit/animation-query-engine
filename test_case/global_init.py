from includes import Logger
import json

GLOBAL_CONFIG = './app.config'

def init(sys,app):
	
	# config constant
	PORT_MAIN, PORT_QE, PORT_SH, PORT_LR = [int(port) for port in sys.argv[1:]]
	SERVICE_NAME = sys.argv[0]

	with open(GLOBAL_CONFIG , 'r' , encoding='=utf-8') as f:
		configs = json.load(f)
	for k,v in configs.items():
		Logger.printLog(f"{SERVICE_NAME}: GLOBAL INIT CONFIG {k}: {v}")
		app.config[k]=v
	return app,PORT_MAIN, PORT_QE, PORT_SH, PORT_LR






