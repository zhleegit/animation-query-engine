echo "=== current processes ==="
ps -a | grep 'test_case\/microservice_.*py'
ps -a | grep 'test_case\/microservice_.*py' | awk '{ print $1}' | xargs -n 1 -I {} kill {}
echo "=== after killing ==="
sleep 2
ps -a | grep 'test_case\/microservice_.*py'
echo "done"
