from flask import Flask, render_template, jsonify, request, redirect
import json, os, shutil, numpy as np
from includes import LabelRecommender as LR
from includes import global_init

from os.path import join as pj
import includes.Logger as Logger
import sys

QE_initialized = False
app = Flask(__name__)
app, PORT_MAIN, PORT_QE, PORT_SH, PORT_LR = global_init.init(sys,app)



@app.route('/lr/pred_tag/<img_name>')
def api_pred_tag(img_name):
    img_src = pj(app.config["DIR_RUNTIME_ANIM"], img_name)

    Logger.printLog(f"receive recommend input : {img_src}")
    tags = LR.runtime_pred_tag_from_src(img_src)
    status = {}
    status['status'] = 'success' if type(tags) is list and len(tags)>0 else 'fail'
    return jsonify(tags)

@app.route('/lr/update_model')
def api_update_model():
    """
    auto load from LR.RUNTIME_SRC_TAGS and train model
    """
    LR.runtime_finetune()




if __name__ == "__main__":
    LR.runtime_load_model()
    app.run(debug=True, host="127.0.0.1", port=8083)
