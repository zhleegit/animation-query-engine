echo "rm upload and outputs"
if test -d uploads; then
    rm -rf uploads
fi
if test -d outputs; then
    rm -rf outputs
fi

echo "rm old includes, datasource, auto_info, ";

if test -d includes; then
    rm -rf includes
fi
if test -d datasource; then
    rm -rf datasource
fi
if test -d auth_info; then
    rm -rf auth_info
fi


echo "initialize all files"
mkdir outputs
mkdir -p uploads/slides
mkdir -p uploads/anims
cp -r ./../includes .
cp -r ./../datasource .
cp -r ./../auth_info .
source ./auth_info/set_key_path.sh
source ./auth_info/test_credential.sh

echo "start QE serice"
python microservice_QE.py &
echo "start SH serice"
python microservice_SH.py &
echo "start LR serice"
python microservice_LR.py &
echo "start Main serice"
python microservice_main.py & 


