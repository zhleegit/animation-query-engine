from flask import Flask, render_template, jsonify, request, redirect
import requests
import json, os, shutil, numpy as np
from os.path import join as pj
import includes.slideHelper as SH
import includes.Logger as Logger
import sys
import traceback
from includes import global_init



app = Flask(__name__)
app, PORT_MAIN, PORT_QE, PORT_SH, PORT_LR = global_init.init(sys,app)




# config constant
DEBUG = True
URL_QE = f"http://127.0.0.1:{PORT_QE}"
URL_ENGPOINT_TEXTS_TO_SRCS = f"{URL_QE}/texts_to_srcs"
DEFAULT_SRCS = [
    "datasource/runtime/anim_repo/amy_run_gif.gif",
    "datasource/runtime/anim_repo/amy_run_sad_gif.gif",
]


def get_srcs_from_QE(texts):
    """delegate texts_to_srcs from QE with RPC"""
    return_default = False

    # post request to QE server
    req_content = {"texts": texts}
    r = requests.post(URL_ENGPOINT_TEXTS_TO_SRCS, json=req_content)

    # request checking
    if r.status_code != 200:
        Logger.printLog(f"SH: status code: {r.status_code}, return default srcs")
        return DEFAULT_SRCS
    rsp = r.json()
    if rsp.get("srcs") is None or not return_default:
        Logger.printLog("SH: response a None json")
        return DEFAULT_SRCS

    # todo: can add length check to make sure response returns K srcs

    Logger.printLog("SH: receive srcs from QE")
    print(rsp["srcs"])
    return rsp["srcs"]


def gen_v1(slide_path):

    # todo: need to change output name dynamically
    slide_path_out = pj(app.config["DIR_OUTPUTS"], "sample_output.pptx")
    try:
        Logger.printLog("SH: configure text processors")
        text_processors = {"texts_to_srcs": get_srcs_from_QE}

        Logger.printLog("SH: start generation procedure")
        # inject text processors
        SH.generate_ppt_from_2D_list(
            fn_in=slide_path, fn_out=slide_path_out, text_processors=text_processors
        )

        Logger.printLog(f"Done for {slide_path}")
        return True
    except:
        print(traceback.format_exc())
        return False


@app.route("/gen_v1/<slide_name>")
def api_gen_v1(slide_name):
    slide_path = pj(app.config["DIR_UPLOAD_SLIDE"], slide_name)
    if not os.path.exists(slide_path):
        return redirect(f"http://127.0.0.1:{PORT_MAIN}/list_slides")
    status = {}
    status["status"] = "success" if gen_v1(slide_path) else "fail"
    # todo : implement return to main server with status parameter
    return jsonify(status)


if __name__ == "__main__":
    SH.show_log = DEBUG
    app.run(debug=DEBUG, host="127.0.0.1", port=PORT_SH)
