# Miscoservice deployment

## Note
- make sure your localhost ports 8080, 8081, 8082 are free
- make sure you are using MacOS
- make sure you have installed all packages

## Architecture
### (Previous use) Monolithic 
In monolithic main.py, You may use following architecture for test and can run program only one time.
> the `main.py` has been removed because of reversion of microservice arthitecture

![](../assets/monolithic_arch.jpg)


### (Current) Microservice
In this architecture, we decouple the system from origin.
you can test each subsystem without quit whole services and 
can easily extend with additional services.

![](../assets/microservice_arch.jpg)
## Usage

### deploy (MacOS)
`bash deploy.sh`

### operations by browser
index: `localhost:8080`
- upload files (will be uploaded to `uploads/slides`)

list slides: `localhost:8080/list_slides` 
- list uploaded slides
- clicking slide's link will start slide generaiton procedure

