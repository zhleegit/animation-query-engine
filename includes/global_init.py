from includes import Logger
import json


GLOBAL_CONFIG = './app.config'
PORT_MAIN , PORT_QE , PORT_SH , PORT_LR = [None]*4

def init_port(port_dict):
	
	for port_type , num in port_dict.items():
		if port_type=='MAIN':
			PORT_MAIN = int(num)
		elif port_type=='QE':
			PORT_QE = int(num)
		elif port_type=='SH':
			PORT_SH = int(num)
		elif port_type=='LR':
			PORT_LR = int(num)
	return PORT_MAIN, PORT_QE , PORT_SH ,PORT_LR

def init(sys,app):
	global PORT_MAIN , PORT_QE , PORT_SH , PORT_LR
	
	# config constant
	SERVICE_NAME = sys.argv[0]

	with open(GLOBAL_CONFIG , 'r' , encoding='=utf-8') as f:
		configs = json.load(f)
	for k,v in configs.items():
		if k=='PORTS':
			PORT_MAIN, PORT_QE, PORT_SH, PORT_LR = init_port(v)
		Logger.printLog(f"{SERVICE_NAME}: GLOBAL INIT CONFIG {k}: {v}")
		app.config[k]=v
	return app,PORT_MAIN, PORT_QE, PORT_SH, PORT_LR






