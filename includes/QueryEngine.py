from sys import set_coroutine_origin_tracking_depth
from typing import KeysView
from includes import wordEmbeddingHelper as WE, Logger


# import initializer
# import Logger
import numpy as np
import json
import shutil
import os
import six
from random import shuffle
from os.path import join as pj
import pandas as pd
import six
from google.cloud import translate_v2 as translate



show_log = True
path_meta_fn2tag = "./meta_fn2tags.json"
path_meta_tags2feat = "./meta_tags2feat.json"
path_meta_tags2fn = "./meta_tags2fn.json"

gender = None
meta_fn2tags, meta_tag2feat, meta_tags2feat, meta_tags2fn = None, None, None, None


TOP_K = None



IN_DISK_DIR = pj("datasource", "init")
IN_DISK_TABLE_TAG_FN = pj(IN_DISK_DIR, "table_tag_fn.csv")
IN_DISK_DATASET = pj(IN_DISK_DIR, "init_dataset")

RUNTIME_DIR = pj("datasource", "runtime")
RUNTIME_ANIM_REPO = pj(RUNTIME_DIR, "anim_repo")
RUNTIME_SRC_TAGS = pj(RUNTIME_DIR, "src_tags.json")
# stores BERT cache
RUNTIME_TAG_FEAT = pj(RUNTIME_DIR, "tag_feat.json")
# map tag to multi srcs
RUNTIME_TAG_SRCS = pj(RUNTIME_DIR, "tag_srcs.json")

runtime_map_src_tags = None
runtime_map_tag_feat = None
runtime_map_tag_srcs = None


## runtime
def runtime_init(top_k=3):
    global runtime_map_src_tags, runtime_map_tag_feat, runtime_map_tag_srcs, TOP_K
    with open(RUNTIME_SRC_TAGS, "r", encoding="utf-8") as f:
        runtime_map_src_tags = json.load(f)
    with open(RUNTIME_TAG_FEAT, "r", encoding="utf-8") as f:
        runtime_map_tag_feat = json.load(f)
    with open(RUNTIME_TAG_SRCS, "r", encoding="utf-8") as f:
        runtime_map_tag_srcs = json.load(f)
    TOP_K = top_k

    Logger.printLog("reload maps")


def runtime_init_datasource(top_k=3):
    global runtime_map_src_tags, runtime_map_tag_feat, runtime_map_src_tags, TOP_K

    src_tags = {}
    tag_feat = {}

    # rm old anim_repo
    check_clean_runtime()

    # create runtime repo structure
    os.mkdir(RUNTIME_DIR)
    os.mkdir(RUNTIME_ANIM_REPO)

    df = pd.read_csv(IN_DISK_TABLE_TAG_FN)
    fns = df["fn"].values.tolist()
    tags = df["tags"].values.tolist()

    # the stage has normailized the tag to English
    tags_english = []
    for tag_raw in tags:
        tag_list = translate_text_list(tag_raw.split("_"))
        tags_english.append(tag_list)

        # update tag-to-feat
        for tag_word_english in tag_list:
            if tag_word_english in tag_feat:
                continue

            # only non-array format can be saved as json
            tag_feat[tag_word_english] = WE.get_feature(tag_word_english, array=False)

    # cp from in-disk to runtime-repo
    # src-to-tag : runtime-repo path to tag
    for i in range(len(fns)):
        src = os.path.join(IN_DISK_DATASET, fns[i])
        src_runtime = os.path.join(RUNTIME_ANIM_REPO, fns[i])
        shutil.copy2(src, src_runtime)
        src_tags[src_runtime] = tags_english[i]

    # asign to global map
    runtime_map_src_tags = src_tags
    runtime_map_tag_feat = tag_feat
    runtime_map_tag_srcs = dict()
    for src, tags in runtime_map_src_tags.items():
        for tag in tags:
            if tag not in runtime_map_tag_srcs:
                runtime_map_tag_srcs[tag] = []
            if src in runtime_map_tag_srcs[tag]:
                continue
            runtime_map_tag_srcs[tag].append(src)

    with open(RUNTIME_TAG_FEAT, "w", encoding="utf-8") as f:
        json.dump(runtime_map_tag_feat, f)
    Logger.printLog(f"Init {RUNTIME_TAG_FEAT}")
    with open(RUNTIME_SRC_TAGS, "w", encoding="utf-8") as f:
        json.dump(runtime_map_src_tags, f)
    Logger.printLog(f"Init {RUNTIME_SRC_TAGS}")
    with open(RUNTIME_TAG_SRCS, "w", encoding="utf-8") as f:
        json.dump(runtime_map_tag_srcs, f)
    Logger.printLog(f"Init {RUNTIME_SRC_TAGS}")

    TOP_K = top_k


def check_clean_runtime():
    if not os.path.exists(RUNTIME_DIR):
        return
    Logger.printLog(
        f"rm old {RUNTIME_DIR}'+\
                    ' and create initizlize one"
    )
    shutil.rmtree(RUNTIME_DIR)
    return


def translate_text(text, target="en"):
    """
    target must be ISO 639-1 language code
    """
    translate_client = translate.Client()

    if isinstance(text, six.binary_type):
        text = text.decode("utf-8")

    result = translate_client.translate(text, target_language=target)
    Logger.printLog(
        " translation info \n"
        + "input: {}\n".format(result["input"])
        + "detected source language: {}".format(result["detectedSourceLanguage"])
    )
    return result["translatedText"]


def translate_text_list(text_list, target="en"):
    """
    target must be ISO 639-1 language code
    """
    results = []
    translate_client = translate.Client()
    Logger.printLog(" translation info (list) ")
    for i, text in enumerate(text_list):
        if isinstance(text, six.binary_type):
            text = text.decode("utf-8")

        result = translate_client.translate(text, target_language=target)
        Logger.printLog(
            "input: {}\n".format(result["input"])
            + "detected source language: {}".format(result["detectedSourceLanguage"])
        )
        results.append(result["translatedText"])
    return results


def runtime_get_topk_pairs_from_multi_sentences(sentences, k=3, rand_range=0):
    all_slide_pairs = []
    sentences_eng = translate_text_list(sentences)
    for sentence_eng in sentences_eng:
        # get all tags corresponding to "one slide"
        # one slide can contains multiple sentences
        all_slide_pairs += runtime_get_same_topk_pairs(sentence_eng)

    topk_src_scores = get_topk_src_from_slide_pairs(all_slide_pairs)

    if show_log:
        Logger.printLog("top of top")
        for i,sentence in enumerate(sentences):
            Logger.printLog(f'input:\t {sentence} -> (Eng) {sentences_eng[i]}')
        for pair in topk_src_scores:
            Logger.printLog(f"{pair[0]}: {pair[1]}")

    # one tag may map to multiple paths (see )
    # listOfList = [runtime_map_src_tags.get(pair[0]) for pair in all_slide_pairs]

    result = [pair[0] for pair in topk_src_scores]
    # if gender is None:
    #     gender_filter(all_slide_pairs[0][0])
    #     Logger.printLog("Gender filter start with " + all_slide_pairs[0][0])
    return result


def runtime_get_same_topk_pairs(sentence):
    """
    return [same pairs + topk non-same pairs]
    """

    target_feat = WE.get_feature(sentence)
    sim_pairs = get_sim_pairs(target_feat)
    assert (
        0 < TOP_K <= len(sim_pairs)
    ), f"k should be smaller than all db size {len(sim_pairs)}"
    same_pairs, non_same_pairs = same_tag_filter(sim_pairs)

    # same tag shall be retrieved
    same_pairs_len = len(same_pairs)

    # NOT always "K" but includes "same pairs"
    topk_pairs = same_pairs + non_same_pairs[: TOP_K - same_pairs_len]

    if show_log:
        Logger.printLog("top k pairs with sim score")
        for concatTag, score in topk_pairs:
            Logger.printLog(f'"{concatTag}":\t{score}')

    return topk_pairs


def get_sim_pairs(target_feat):
    sim_pairs = []
    for tag, feat in runtime_map_tag_feat.items():
        sim_pairs.append([tag, get_sim(feat, target_feat)])
    sim_pairs = sorted(sim_pairs, key=lambda x: -x[1])
    return sim_pairs


def same_tag_filter(sim_pairs):
    same_pairs = []
    non_same_pairs = []
    for pair in sim_pairs:
        if pair[1] > 0.999:
            same_pairs.append(pair)
        else:
            non_same_pairs.append(pair)
    return same_pairs, non_same_pairs


def get_topk_src_from_slide_pairs(all_slide_pairs):
    """ tagged-source selection algorithm """
    src_total_score = dict()
    src_total_score_list = []
    for tag, score in all_slide_pairs:
        srcs = runtime_map_tag_srcs[tag]
        for src in srcs:

            # calculate src score using SUM rather than AVG 
            #  e.g. match 2 `similar tags` is better than match 1 `same tag`
            src_total_score[src] = (
                src_total_score[src] + score 
                if src_total_score.get(tag) is not None
                else score
            )

    for src, score in src_total_score.items():
        src_total_score_list.append([src, score])
    src_total_score = sorted(src_total_score_list, key=lambda x: -x[1])
    return src_total_score_list[:TOP_K]


def get_sim(vec1, vec2):
    return np.dot(vec1, vec2) / (np.linalg.norm(vec1) * np.linalg.norm(vec2))
