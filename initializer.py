import pandas as pd
import os , json, shutil, sys
from utils import Logger
from includes import languageNormalizer as LN
from includes import wordEmbeddingHelper as WE
from tqdm import tqdm


## Usage ################################
# 
#  dependencies:
#   - datasource/init/*
#   - datasource/init/table_tab_fn.csv
#   - (old or none) datasource/anim_repo 
#   - (old or none) datasource/src_tags.json
#  run:
#   - (prerequisite)
#     modifying datasource/init/table_tab_fn.csv, and
#     make sure datasource/init/* has corresponding animation 
# 
#  output:
#   - updated datasource/anim_repo/*
#   - updated datasource/init/src_tags.json
# 
#########################################


TAG_PATH = os.path.join('datasource','init','table_tags_fn.csv')
INIT_DATA_DIR = os.path.join('datasource','init' , 'init_dataset')

# after moving init data to OUT_DATA_DIR,
# read from OUT_DATA_overrite current meta.json if exists
SRC_TAG = os.path.join('datasource', 'src_tags.json')
REPO_DIR = os.path.join('datasource' , 'anim_repo')

# output meta
fn_tags = {}

# space separated str
concatTag_fn = {}
concatTag_feat = {}

def init_fn_tags():
    global fn_tags

    # rm old anim_repo
    if os.path.exists(REPO_DIR):
        Logger.printLog(f"rm old {REPO_DIR} and create from init datasource")
        shutil.rmtree(REPO_DIR)
        os.mkdir(REPO_DIR)

    df = pd.read_csv(TAG_PATH)
    fns = df['fn'].values.tolist()
    tags = df['tags'].values.tolist()
    tags = [LN.translate_text_list(tag.split('_')) for tag in tags]
    for i in range(len(fns)):
        src = os.path.join(INIT_DATA_DIR , fns[i])
        src_repo = os.path.join(REPO_DIR, fns[i])
        shutil.copy2(src, src_repo)

        fn_tags[src_repo] = tags[i] 


        # concatTag=' '.join(tags[i])
        # if concatTag not in concatTag_fn:
        #     concatTag_fn[concatTag]=[]
        # concatTag_fn[concatTag].append(fn)
        # if concatTag not in concatTag_feat:
        #     concatTag_feat[concatTag]=wes.get_feature(concatTag, array=False)


    with open(SRC_TAG , 'w' , encoding='utf-8') as f:
        json.dump( fn_tags, f)
    # with open('meta_tags2fn.json' , 'w' , encoding='utf-8') as f:
    #     json.dump( concatTag_fn, f)
    # with open('meta_tags2feat.json' , 'w' , encoding='utf-8') as f:
    #     json.dump( concatTag_feat, f)
    Logger.printLog(f"Init {SRC_TAG}")


