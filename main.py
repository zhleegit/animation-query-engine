from random import Random
import includes.QueryEngine as QE
import includes.slideHelper as SH
import includes.LabelRecommender as LR
import os
import shutil
import numpy as np
import sys
import includes.Logger as Logger

QE.show_log = False
SH.show_log = False
TOP_K = 3
QE_INIT_DATASOURCE = False
MAIN_SLIDE_NAME = None

def main():

    assert os.path.exists(MAIN_SLIDE_NAME), f"{MAIN_SLIDE_NAME} not found"

    if QE_INIT_DATASOURCE:
        QE.runtime_init_datasource()
    else:
        QE.runtime_init()

    Logger.printLog('configure text processors')
    text_processors={
                    SH.TAG_TEXT_TO_SRC:QE.runtime_get_topk_pairs_from_multi_sentences
                    }


    Logger.printLog('start generation procedure')
    # inject text processors
    SH.generate_ppt_from_2D_list(fn_in=MAIN_SLIDE_NAME,
                                text_processors=text_processors)
    
    Logger.printLog(f"Done for {MAIN_SLIDE_NAME}")
    return None



if __name__ == "__main__":
    if len(sys.argv) != 2:
        Logger.printLog("No source ppt specified")
        exit(1)
    source_ppt = sys.argv[1]

    Logger.printLog(f"source ppt: {source_ppt}")
    main(source_ppt)
