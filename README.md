# animation query engine 

## usage
### set auth
cd to `auth_info`
set according to your system



## QueryEngine

### runtime functions

- def runtime_init_datasource(top_k=3)
used for first time

- def runtime_init(top_k=3)
initialize datasource from initial repo (`datasource/init`) into runtime repo(`datasource/runtime`)

- def runtime_get_topk_pairs_from_multi_sentences(sentences, k=3, rand_range=0)
1) retrieve top-k image sources according to "sentences"
2) sentences: all sentences in one slide page
3) used below function `runtime_get_same_topk_pairs` for each sentence

- def runtime_get_same_topk_pairs(sentence)
same function as function above but used single sentence

### description

This module aims at retrieving animations corresponding to texts parsed from each slide. After initialization, it maintains three mapping tables, `runtime_map_*`, including 
- src to tags: 
- tag to srcs: retrieve animations by tag
- tag to feat: used as a cache mechnism which transforms tag to text feature produced by normalizing text to English form and extracting the output from BERT language model in order to increase performance while querying the top-k matched tags

![](/assets/map_relations.png)

#### Query for matched tags
When query function, `get_topk_pairs_from_multi_sentences`, is called, each text from one slide will be transformed into feature vectors by applying Google's translation API and BERT feature extractor, we then find the **matched tags**, which includes `same tag` as the text and the `top-k matched tags` computed from Cosine-similarity measure, . The matched tags for each text we mention abve will be used to apply **tagged-source selection algorithm** in order to find the `top-k animations` for the corresponding slide.

$tags_{match} = tags_{same} \cup tags_{top-k}$

#### Query for top-k animation source
To recommend the top-k animation for one slide, we shall consider two conditions. the first, one slide may contain multiple texts, and therefore we cannot make recommendation by only one text; secondly, most of animations in the repository of the CMS are tagged with more than one label, when the text matches multiple tags pointing to the same animation, we want the animation has higher probability to be recommended than those with less tags matched. We then propose **Tagged Source selection algorithm**, shown in follow:

Assume there are $N$ texts in one slide and none of the text is the same to any tag in the CMS repository, which  means $tags_{match}=tags_{top-k}$ and $|tags_{match}|=k$.

According to our query mechnism, each text will be used to find $k$ matched tags with corresponding similarity scores, therefore the $i$-th text becomes $\{(tag_{i,1},S_{i,1}), ... , (tag_{i,k},S_{i,k})\}$. We then use inversion table to find all animation sources correcponding to these $tag_{i,j}$ where $i=\{1,...,N\},\ j=\{1,...,k\}$. For each animation, we calculate the total score by sumation of single similarity score accumulated from the matched tags, that is $TotalScore_{animation_n}=\sum_{tag_{i,j}\in tag_{animation}}S_{i,j}$, then sort all $animation_n$ by decending order and get the first $k$ animations as our top-k animations for recommendation of an input slide. The result shows this algorithm has the ability to recommend suitable animations for the slide with multiple texts.